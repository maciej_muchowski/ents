package pl.codementors.ents;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import pl.codementors.ents.model.Ent;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EntsList implements Initializable {

    @FXML
    private TableView<Ent> entsTable;

    @FXML
    private TableColumn<Ent, Integer> yearColumn;

    @FXML
    private TableColumn<Ent, Integer> heightColumn;

    @FXML
    private TableColumn<Ent, String> gradeColumn;

    @FXML
    private TableColumn<Ent, Ent.Type> typeColumn;

    @FXML
    private ProgressBar progressBar;

    private ResourceBundle rb;

    private static final Logger log = Logger.getLogger(EntsList.class.getCanonicalName());


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        rb = resourceBundle;
        Ent ent1 = new Ent(1952, 30, "Spruce", Ent.Type.CONIFEROUS);
        Ent ent2 = new Ent(1995, 12, "Maple", Ent.Type.LEAFY);
        Ent ent3 = new Ent(1939, 25, "Chestnut", Ent.Type.LEAFY);

//       ObservableList<Ent> entList = FXCollections.observableArrayList();
//        entList.addAll(ent1,ent2,ent3);
//        entsTable.setItems(entList);
    }

    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void saveFile(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(null);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(new ArrayList<>(entsTable.getItems()));
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    @FXML
    private void openFile(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            List<Ent> ents = (List<Ent>) ois.readObject();
            entsTable.setItems(FXCollections.observableArrayList(ents));
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    @FXML
    private void handlePlant(ActionEvent event) {
        Ent ent = new Ent();
        dialogSetYearOfPlanting(ent);
        dialogSetHeight(ent);
        dialogSetGrade(ent);
        dialogSetType(ent);
        entsTable.getItems().add(ent);
    }

    @FXML
    private void handleCut(ActionEvent event) {
        int selectedRow = entsTable.getSelectionModel().getSelectedIndex();
        if (selectedRow >= 0) {
            entsTable.getItems().remove(selectedRow);
        } else {
            alertNoSelected();
        }
    }

    @FXML
    private void handleRootTheSeedling(ActionEvent event) {
        int selectedRow = entsTable.getSelectionModel().getSelectedIndex();
        if (selectedRow >= 0) {
            String parentGrade = entsTable.getItems().get(selectedRow).getGrade();
            String parentType = entsTable.getItems().get(selectedRow).getType().name();
            int height = 10;
            int yearOfPlanting = Calendar.getInstance().get(Calendar.YEAR);
            Ent ent = new Ent(yearOfPlanting, height, parentGrade, Ent.Type.valueOf(parentType));
            entsTable.getItems().add(ent);
        } else {
            alertNoSelected();
        }
    }


    private void dialogSetYearOfPlanting(Ent ent) {
        TextInputDialog input = new TextInputDialog("");
        input.setTitle(rb.getString("dialogPlantTitle"));
        input.setHeaderText("");
        input.setContentText(rb.getString("dialogPlantEnterYear"));
        int year = Integer.parseInt(input.showAndWait().orElse(""));
        ent.setYearOfPlanting(year);
    }

    private void dialogSetHeight(Ent ent) {
        TextInputDialog input = new TextInputDialog("");
        input.setTitle(rb.getString("dialogPlantTitle"));
        input.setHeaderText("");
        input.setContentText(rb.getString("dialogPlantEnterHeight"));
        int height = Integer.parseInt(input.showAndWait().orElse(""));
        ent.setHeight(height);
    }

    private void dialogSetGrade(Ent ent) {
        TextInputDialog input = new TextInputDialog("");
        input.setTitle(rb.getString("dialogPlantTitle"));
        input.setHeaderText("");
        input.setContentText(rb.getString("dialogPlantEnterGrade"));
        String grade = input.showAndWait().orElse("");
        ent.setGrade(grade);
    }

    private void dialogSetType(Ent ent) {
        List<String> types = Arrays.asList(Ent.Type.LEAFY.name(), Ent.Type.CONIFEROUS.name());
        ChoiceDialog<String> choice = new ChoiceDialog<>(null, types);
        choice.setTitle(rb.getString("dialogPlantTitle"));
        choice.setHeaderText("");
        choice.setContentText(rb.getString("dialogPlantSelectType"));
        String type = choice.showAndWait().orElse("");
        ent.setType(Ent.Type.valueOf(type));
    }

    private void alertNoSelected() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(rb.getString("alertNoSelectionTitle"));
        alert.setHeaderText(rb.getString("alertNoSelectionHeader"));
        alert.setContentText(rb.getString("alertNoSelectionContent"));
        alert.showAndWait();
    }
}
