package pl.codementors.ents;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ResourceBundle;

public class CopseMain extends Application {
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ents_list.fxml"),
                ResourceBundle.getBundle("locales"));

        Scene scene = new Scene(root, 600, 600);
        scene.getStylesheets().add("/styles.css");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
