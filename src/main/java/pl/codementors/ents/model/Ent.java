package pl.codementors.ents.model;

import java.io.Serializable;
import java.util.Objects;

public class Ent implements Serializable {
    public enum Type {
        CONIFEROUS,
        LEAFY;
    }

    private int yearOfPlanting;

    private int height;

    private String grade;

    private Type type;

    public Ent() {
    }

    public Ent(int yearOfPlanting, int height, String grade, Type type) {
        this.yearOfPlanting = yearOfPlanting;
        this.height = height;
        this.grade = grade;
        this.type = type;
    }

    public int getYearOfPlanting() {
        return yearOfPlanting;
    }

    public void setYearOfPlanting(int yearOfPlanting) {
        this.yearOfPlanting = yearOfPlanting;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ent ent = (Ent) o;
        return yearOfPlanting == ent.yearOfPlanting &&
                height == ent.height &&
                Objects.equals(grade, ent.grade) &&
                type == ent.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(yearOfPlanting, height, grade, type);
    }

    @Override
    public String toString() {
        return "Ent{" +
                "yearOfPlanting=" + yearOfPlanting +
                ", height=" + height +
                ", grade='" + grade + '\'' +
                ", type=" + type +
                '}';
    }
}
